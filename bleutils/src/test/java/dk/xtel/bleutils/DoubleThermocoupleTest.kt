/*
 * Copyright (c) 2018, Xtel Wireless ApS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package dk.xtel.bleutils

import dk.xtel.bleutils.bledecoder.XtelBeaconDoubleThermoCouple
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class DoubleThermocoupleTest {

    private var decoder = XtelBeaconDoubleThermoCouple()

    @Test
    fun decode_known_good_data() {
        val data: ByteArray = byteArrayOf(
            0xDE.toByte(),
            0xFA.toByte(),
            0x16.toByte(),
            0x1.toByte(),
            0x2.toByte(),
            0x0.toByte(),
            0x0.toByte(),
            0xED.toByte(),
            0xCB.toByte(),
            0x3F.toByte(),
            0x3D.toByte(),
            0x2.toByte(),
            0x37.toByte(),
            0x1.toByte(),
            0x0.toByte(),
            0x0.toByte(),
            0x0.toByte(),
            0x0.toByte(),
            0xB4.toByte(),
            0x8F.toByte(),
            0x26.toByte()
        )
        var result = decoder.decode(data)
        assertEquals("BeaconDoubleThermoCouple", result?.getString("sensor_type"))
    }
}
