/*
 * Copyright (c) 2018, Xtel Wireless ApS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package dk.xtel.bleutils.bledecoder

/**
 * Base class for Xtel Beacons, implementing common functionality like sensor identification
 */
open class XtelBaseBeacon : ICodec {
    open val FRAME_TYPE: Byte = 1
    open val CONFIGURATION_TYPE: Byte = 0

    /**
     * Generic implementation that check for Xtel Beacons standard headers
     * Sub classes can override this method if they need to, this implementation
     * will cover most, if not all, the Xtel Beacons.
     */
    override fun canHandle(data: ByteArray): Boolean {
        if (!verifyHeader(data))
            return false
        return true
    }


    /**
     * Check the header to see if it is one of ours
     */
    fun verifyHeader(data: ByteArray): Boolean {
        if (data[0] == 0x16.toByte()) {
            if (data[1] != 0xDE.toByte()) {
                return false
            }
            if (data[2] != 0xFA.toByte()) {
                return false
            }
            if (data[3] != FRAME_TYPE) {
                return false
            }
            if (data[4] != CONFIGURATION_TYPE) {
                return false
            }
        }  else {
            return false
        }
        return true
    }
}