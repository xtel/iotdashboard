/*
 * Copyright (c) 2018, Xtel Wireless ApS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package dk.xtel.bleutils.bledecoder

import org.json.JSONObject

/**
 * Interface that actual codecs need to implement
 */
interface ICodec {
    /**
     * Ask this codec if it can decode some data
     *
     * @params data (ByteArray) raw data
     * @return true if this codec can decode the data, false otherwise
     */
    fun canHandle(data: ByteArray):Boolean {
        return false
    }

    /**
     * Decode the given raw data and return a JSONObject with the sensor specific
     * data
     *
     * @params data (ByteArray) raw data
     * @return JSONObject with sensor data, null if data cannot be decoded
     */
    fun decode(data: ByteArray):JSONObject? {
        return null
    }
}