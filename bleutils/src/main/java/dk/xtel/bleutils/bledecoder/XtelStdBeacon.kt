/*
 * Copyright (c) 2018, Xtel Wireless ApS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package dk.xtel.bleutils.bledecoder

import org.json.JSONArray
import org.json.JSONObject
import unsigned.toUBigInt
import unsigned.toUInt
import unsigned.toUbyte
import unsigned.toUint
import java.nio.ByteBuffer

class XtelStdBeacon : XtelBaseBeacon() {
    override val CONFIGURATION_TYPE: Byte = 0x0C
    override fun canHandle(data: ByteArray): Boolean {
        if (!verifyHeader(data))
            return false
        return true
    }
    override fun decode(data: ByteArray): JSONObject? {
        var res = JSONObject()
        var buffer = ByteBuffer.wrap(data)
        var data_array = JSONArray()

        res.put("data", data_array)
        res.put("sensor_type", "StandardBeacon")
        var data_desc = JSONObject()

        data_desc.put("name", "Custom")
        data_desc.put("unit", "-")
        data_desc.put("value", 0)
        data_array.put(data_desc)
        return res
    }
}