/*
 * Copyright (c) 2018, Xtel Wireless ApS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package dk.xtel.bleutils.bledecoder

import org.json.JSONObject

/**
 * Entry point for decoding Xtel Wireless Beacon data
 * Instantiate this class, pass some Beacon data to that instance's decode() method
 * and you'll get a JSONObject containing the decoded data that you can use to
 * present data, graph it, apply Machine Learning to it and what not.
 *
 *
 */
class CodecManager() {
    // This array contains the various decoders that this build can handle
    private val codecs = arrayOf(
            XtelBeaconDoubleThermoCouple()
    )

    /**
     * Decode the given data by iterating through the registered codec array and returning
     * data from the codec that can decode it
     * @param data (ByteArray) the data to decode
     * @return JSONObject? valid JSONObject containing decoded data or null if no codec could handle
     * that data :(
     */
    fun decode(data: ByteArray): JSONObject? {
        val codec = getDecoder(data)
        return codec?.decode(data)
    }

    /**
     * Find and return the codec implementation capable of decoding the given data
     * @param data (ByteArray) the data to decode
     * @return ICodec? the instance capable of decoding the data or null if none was found :(
     */
    private fun getDecoder(data: ByteArray): ICodec? {
        for(codec in codecs) {
            if(codec.canHandle(data)) {
                return codec
            }
        }
        return null
    }
}