/*
 * Copyright (c) 2018, Xtel Wireless ApS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package dk.xtel.bleutils.bledecoder

import org.json.JSONArray
import org.json.JSONObject
import unsigned.toUInt
import java.nio.ByteBuffer

/**
 * Decode data from an Xtel Beacon that can measure:
 * - atmospheric pressure
 * - relative humidity
 * - ambient temperature
 * - 2 thermo-couplers
 * - advertisement counts
 */
class XtelBeaconDoubleThermoCouple : XtelBaseBeacon() {
    override val CONFIGURATION_TYPE: Byte = 2

    /**
     * This method decodes raw data from an Xtel Double Thermocouple beacon
     * The data layout is as follows:
     * - bytes 7, 8 and 9: 24 bits raw pressure data
     * - bytes 10, 11: 16 bits raw humidity
     * - bytes 12, 13: 16 bits raw temperature
     * - bytes 14, 15: 16 bits raw 1st thermocouple data
     * - bytes 17, 18: 16 bits raw 2nd thermocouple data
     * - bytes 18, 19 and 20: 24 bits advertisement count
     */
    override fun decode(data: ByteArray): JSONObject? {
        var res = JSONObject()
        var buffer = ByteBuffer.wrap(data)
        var data_array = JSONArray()

        res.put("data", data_array)
        res.put("sensor_type", "BeaconDoubleThermoCouple")
        var data_desc = JSONObject()

        // Handle pressure
        var raw_presssure = ((buffer.get(9).toUInt() shl 24) or (buffer.get(8).toUInt() shl 16) or (buffer.get(7).toUInt() shl 8))
        var pressure = raw_presssure.toDouble() / (256 * 4096)
        pressure =  Math.round(pressure * Math.pow(10.0, 2.0)) / Math.pow(10.0, 2.0)
        data_desc.put("name", "pressure")
        data_desc.put("icon", "pressure")
        data_desc.put("unit", "mPascal")
        data_desc.put("value", pressure)
        data_array.put(data_desc)

        // Handle humidity
        var raw_humidity = ((buffer.get(11).toUInt() shl 8) or buffer.get(10).toUInt())
        var humidity = raw_humidity.toDouble() / 10
        data_desc = JSONObject()
        data_desc.put("name", "relative humidity")
        data_desc.put("icon", "humidity")
        data_desc.put("unit", "%")
        data_desc.put("value", humidity)
        data_array.put(data_desc)

        // Handle temperature
        var raw_temperature = ((buffer.get(13).toUInt() shl 8) or buffer.get(12).toUInt())
        var temperature = raw_temperature.toDouble() / 10
        data_desc = JSONObject()
        data_desc.put("name", "temperature")
        data_desc.put("icon", "temperature")
        data_desc.put("unit", "C")
        data_desc.put("value", temperature)
        data_array.put(data_desc)

        // Handle thermo-couple 1
        var raw_tc1= ((buffer.get(15).toInt() shl 8) or buffer.get(14).toInt())
        var tc1 = raw_tc1.toDouble() / 10
        data_desc = JSONObject()
        data_desc.put("name", "thermocouple")
        data_desc.put("icon", "temperature")
        data_desc.put("unit", "C")
        data_desc.put("value", tc1)
        data_array.put(data_desc)

        // Handle thermo-couple 2
        var raw_tc2= ((buffer.get(17).toInt() shl 8) or buffer.get(16).toInt())
        var tc2 = raw_tc2.toDouble() / 10
        data_desc = JSONObject()
        data_desc.put("name", "thermocouple")
        data_desc.put("icon", "temperature")
        data_desc.put("unit", "C")
        data_desc.put("value", tc2)
        data_array.put(data_desc)

        // Handle advertisement count
        var adv_count = ((buffer.get(20).toUInt() shl 16) or (buffer.get(19).toUInt() shl 8) or buffer.get(18).toUInt())
        data_desc = JSONObject()
        data_desc.put("name", "advertisement count")
        data_desc.put("unit", "Count")
        data_desc.put("value", adv_count)
        data_array.put(data_desc)
        return res
    }
}