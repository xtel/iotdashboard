/*
 * Copyright (c) 2018, Xtel Wireless ApS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package dk.xtel.bleutils.bledecoder

import org.json.JSONArray
import org.json.JSONObject
import unsigned.toUBigInt
import unsigned.toUInt
import unsigned.toUbyte
import unsigned.toUint
import java.nio.ByteBuffer

class XtelBeaconSingleThermoCouple : XtelBaseBeacon() {
    override val CONFIGURATION_TYPE: Byte = 1
    override fun canHandle(data: ByteArray): Boolean {
        if (!verifyHeader(data))
            return false
        return true
    }
    override fun decode(data: ByteArray): JSONObject? {
        var res = JSONObject()
        var buffer = ByteBuffer.wrap(data)
        var data_array = JSONArray()

        res.put("data", data_array)
        res.put("sensor_type", "BeaconSingleThermoCouple")
        var data_desc = JSONObject()

        // Handle pressure
        var raw_presssure = ((buffer.get(9).toUInt() shl 24) or (buffer.get(8).toUInt() shl 16) or (buffer.get(7).toUInt() shl 8))
        var pressure = raw_presssure.toDouble() / (256 * 4096)
        pressure =  Math.round(pressure * Math.pow(10.0, 2.0)) / Math.pow(10.0, 2.0)
        data_desc.put("name", "pressure")
        data_desc.put("icon", "pressure")
        data_desc.put("unit", "mPascal")
        data_desc.put("value", pressure)
        data_array.put(data_desc)

        // Handle humidity
        var raw_humidity = ((buffer.get(11).toUInt() shl 8) or buffer.get(10).toUInt())
        var humidity = raw_humidity.toDouble() / 10
        data_desc = JSONObject()
        data_desc.put("name", "relative humidity")
        data_desc.put("icon", "humidity")
        data_desc.put("unit", "%")
        data_desc.put("value", humidity)
        data_array.put(data_desc)

        // Handle temperature
        var raw_temperature = ((buffer.get(13).toUInt() shl 8) or buffer.get(12).toUInt())
        var temperature = raw_temperature.toDouble() / 10
        data_desc = JSONObject()
        data_desc.put("name", "temperature")
        data_desc.put("icon", "temperature")
        data_desc.put("unit", "C")
        data_desc.put("value", temperature)
        data_array.put(data_desc)

        // Handle thermo-couple
        var raw_tc1= ((buffer.get(15).toInt() shl 8) or buffer.get(14).toInt())
        var tc1 = raw_tc1.toDouble() / 10
        data_desc = JSONObject()
        data_desc.put("name", "thermocouple")
        data_desc.put("icon", "temperature")
        data_desc.put("unit", "C")
        data_desc.put("value", tc1)
        data_array.put(data_desc)

        // Handle advertisement count
        var adv_count = ((buffer.get(18).toUInt() shl 16) or (buffer.get(17).toUInt() shl 8) or buffer.get(16).toUInt())
        data_desc = JSONObject()
        data_desc.put("name", "advertisement count")
        data_desc.put("unit", "Count")
        data_desc.put("value", adv_count)
        data_array.put(data_desc)
        return res
    }
}