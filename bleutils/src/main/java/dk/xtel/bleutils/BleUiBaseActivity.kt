/*
 * Copyright (c) 2018, Xtel Wireless ApS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package dk.xtel.bleutils

import android.Manifest
import android.annotation.TargetApi
import android.bluetooth.BluetoothAdapter
import android.content.*
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.PersistableBundle
import android.provider.Settings
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import dk.xtel.bleutils.BleService
import org.json.JSONObject

abstract class BleUiBaseActivity : AppCompatActivity() {
    private val REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 1

    private var intentFilter = IntentFilter("XTEL-FOB-DATA")
    private lateinit var broadcastReceiver: BroadcastReceiver
    protected var sensors = LinkedHashMap<String, JSONObject>()
    private lateinit var bleServiceIntent:  Intent
    private lateinit var localBroadcastManager: LocalBroadcastManager
    protected var isListening = false
    protected var addresses_filter = ArrayList<String>()
    protected var device_names_filter = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bleServiceIntent = Intent(this, BleService::class.java)
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+ Permission APIs
            requestPermissions();
        }
        localBroadcastManager = LocalBroadcastManager.getInstance(this)
        broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                var sensor = JSONObject(intent?.extras?.getString("fob-data"))
                var mac = sensor.getString("mac_address")
                sensor.put("index", sensors.size)
                var index: Int
                if(sensors.containsKey(mac)) {
                    index = sensors.keys.indexOf(mac)
                    sensors.put(sensor.getString("mac_address"), sensor)
                    onUpdatedSensor(index, sensor)

                } else {
                    index = sensors.size
                    sensors.put(sensor.getString("mac_address"), sensor)
                    onNewSensor(index, sensor)
                }
            }
        }

    }

    protected abstract fun onNewSensor(index: Int, sensor: JSONObject)
    protected abstract fun onUpdatedSensor(index: Int, sensor: JSONObject)
    protected abstract fun onSensorsCleared()

    public fun toggleBle() : Boolean {
        if (isListening) {
            stopBle()
            isListening = !isListening
        } else {
            sensors.clear()
            onSensorsCleared()
            if (startBle()) {
                isListening = !isListening
            }
        }
        return isListening
    }

    public fun startBle(): Boolean {
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
        } else {
            if (!mBluetoothAdapter.isEnabled) {
                btIsTurnedOff()
            } else {
                val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    locationsIsTurnedOff()
                } else {
                    startBleService()
                    return true
                }
            }
        }
        return false
    }

    public fun stopBle() {
        localBroadcastManager.unregisterReceiver(broadcastReceiver)
        this.stopService(bleServiceIntent)
    }

    override fun onPause() {
        if (isListening) {
            stopBle()
        }
        super.onPause()
    }

    override fun onResume() {
        if (isListening) {
            startBle()
        }
        super.onResume()
    }

    override fun onStop() {
        isListening = false
        stopBle()
        super.onStop()
    }

    private fun startBleService() {
        localBroadcastManager.registerReceiver(broadcastReceiver, intentFilter)
        bleServiceIntent.putExtra("addresses", addresses_filter)
        bleServiceIntent.putExtra("device_names", device_names_filter)
        this.startService(bleServiceIntent)
    }

    private fun locationsIsTurnedOff() {
        val builder = android.app.AlertDialog.Builder(this)
        builder.setTitle("Location is currently turned off, do you want to turn it on?")
        builder.setPositiveButton(android.R.string.yes) { dialog, id ->
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(intent);
            dialog.dismiss()
        }
        builder.setNegativeButton(android.R.string.cancel) { dialog, id ->
            dialog.dismiss()
        }
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()
    }

    private fun btIsTurnedOff() {
        val builder = android.app.AlertDialog.Builder(this)
        builder.setTitle("Bluetooth is currently turned off, do you want to turn it on?")
        builder.setPositiveButton(android.R.string.yes) { dialog, id ->
            val intent = Intent(Intent.ACTION_MAIN, null)
            intent.addCategory(Intent.CATEGORY_LAUNCHER)
            val cn = ComponentName("com.android.settings", "com.android.settings.bluetooth.BluetoothSettings")
            intent.component = cn
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivityForResult(intent, 0)
            dialog.dismiss()
        }
        builder.setNegativeButton(android.R.string.cancel) { dialog, id ->
            dialog.dismiss()
        }
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun requestPermissions() {
        val permissionsNeeded = ArrayList<String>()

        val permissionsList = ArrayList<String>()
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("Show Location")

        if (permissionsList.size > 0) {
            if (permissionsNeeded.size > 0) {

                // Need Rationale
                var message = "App need access to " + permissionsNeeded[0]

                for (i in 1 until permissionsNeeded.size)
                    message = message + ", " + permissionsNeeded[i]

                showMessageOKCancel(message,
                        DialogInterface.OnClickListener { dialog, which ->
                            requestPermissions(permissionsList.toTypedArray(),
                                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
                        })
                return
            }
            requestPermissions(permissionsList.toTypedArray(),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
            return
        }
    }

    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show()
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun addPermission(permissionsList: MutableList<String>, permission: String): Boolean {

        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission)
            // Check for Rationale Option
            if (!shouldShowRequestPermissionRationale(permission))
                return false
        }
        return true
    }
}