/*
 * Copyright (c) 2018, Xtel Wireless ApS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package dk.xtel.bleutils

import android.app.Service
import android.bluetooth.BluetoothManager
import android.bluetooth.le.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import dk.xtel.bleutils.bledecoder.CodecManager
import org.json.JSONObject
import java.util.*
import android.os.SystemClock
import android.support.v4.content.LocalBroadcastManager

class BleService : Service() {
    val codec_mgr = CodecManager()
    private lateinit var localBroadcastManager: LocalBroadcastManager
    private lateinit var bleScanner: BluetoothLeScanner
    var bleScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            var index = 0
            val byteSequence = result.scanRecord?.bytes
            if (byteSequence != null) {
                var length = byteSequence[index].toInt()
                index++ // Flag type, 1 byte
                index += length + 1
                length = byteSequence[index].toInt()
                index += length
                index++
                try {
                    val res = codec_mgr.decode(result.scanRecord.bytes.sliceArray(IntRange(index, byteSequence.size - 1)))
                    if (res != null) {
                        val timestamp = (System.currentTimeMillis() - SystemClock.elapsedRealtime() + result.getTimestampNanos() / 1000000)
                        res.put("timestamp", timestamp)
                        res.put("mac_address", result.device.address)
                        res.put("rssi", result.rssi)
                        var intent = Intent("XTEL-FOB-DATA")
                        intent.putExtra("fob-data", res.toString())
                        localBroadcastManager.sendBroadcast(intent)
                    }
                } catch (e: ArrayIndexOutOfBoundsException) {

                }
            }
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startListening(intent)
        localBroadcastManager = LocalBroadcastManager.getInstance(applicationContext)
        return super.onStartCommand(intent, flags, startId)
    }

    override fun stopService(name: Intent?): Boolean {
        bleScanner.stopScan(bleScanCallback)
        return super.stopService(name)
    }

    override fun onBind(intent: Intent?): IBinder? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun startListening(intent: Intent?) {
        val btManager = getApplicationContext().getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bleScanner = btManager.adapter.bluetoothLeScanner
        var addresses = intent?.extras?.getStringArrayList("addresses")
        var device_names = intent?.extras?.getStringArrayList("device_names")
        val scanFilters = ArrayList<ScanFilter>()
        val scanFilter = ScanFilter.Builder()
        addresses?.forEach { address ->
            val scanFilter = ScanFilter.Builder()
            scanFilter.setDeviceAddress(address)
            scanFilters.add(scanFilter.build())
        }
        device_names?.forEach { device_name ->
            val scanFilter = ScanFilter.Builder()
            scanFilter.setDeviceName(device_name)
            scanFilters.add(scanFilter.build())
        }

        //Setup scan settings.
        val scanSettings = ScanSettings.Builder()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            scanSettings.setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE)
            scanSettings.setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
            scanSettings.setNumOfMatches(ScanSettings.MATCH_NUM_MAX_ADVERTISEMENT)
        }
        scanSettings.setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)

        //Start the actual listening for updates from the sensor FOB.
        bleScanner.startScan(scanFilters, scanSettings.build(), bleScanCallback)
    }
}