/*
 * Copyright (c) 2018, Xtel Wireless ApS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package dk.xtel.iotdashboard

import android.os.Bundle
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import dk.xtel.bleutils.BleUiBaseActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : BleUiBaseActivity() {

    private lateinit var rv: RecyclerView

    private lateinit var adapter: SensorAdapter

    public class CapAdapter(val capabilities: JSONArray) :  RecyclerView.Adapter<CapAdapter.CapViewHolder>() {
        public class CapViewHolder(val v: View) : RecyclerView.ViewHolder(v) {
            val cv = itemView.findViewById<CardView>(R.id.cv)
            val sensor_value = itemView.findViewById<TextView>(R.id.sensor_value)
            val sensor_name = itemView.findViewById<TextView>(R.id.sensor_name)
            val sensor_unit = itemView.findViewById<TextView>(R.id.sensor_unit)
            val sensor_icon =itemView.findViewById<ImageView>(R.id.sensor_icon)
        }
        override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): CapViewHolder{
            val v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.capabilities_card, viewGroup, false)
            return CapViewHolder(v)
        }

        override fun getItemCount(): Int {
            return capabilities.length()
        }

        override fun onBindViewHolder(vh: CapViewHolder, i: Int) {
            val data = capabilities.getJSONObject(i)
            vh.sensor_value.text = data.getDouble("value").toString()
            vh.sensor_name.text = data.getString("name")
            vh.sensor_unit.text = data.getString("unit")
            when (data.optString("icon", "xtel_logo")) {
                "xtel_logo" -> {
                    vh.sensor_icon.setImageResource(R.drawable.xtel_logo)
                }
                "temperature" -> {
                    vh.sensor_icon.setImageResource(R.drawable.temperature)
                }
                "humidity" -> {
                    vh.sensor_icon.setImageResource(R.drawable.humidity)
                }
                "pressure" -> {
                    vh.sensor_icon.setImageResource(R.drawable.pressure)
                }
            }
        }
    }
    public class SensorAdapter(val sensors: LinkedHashMap<String, JSONObject>) : RecyclerView.Adapter<SensorAdapter.SensorViewHolder>() {
        var formatter = SimpleDateFormat("HH:mm:ss")
        public class SensorViewHolder(val v: View) : RecyclerView.ViewHolder(v) {
            val cv = itemView.findViewById<CardView>(R.id.cv)
            val mac_address = itemView.findViewById<TextView>(R.id.sensor_mac_address)
            val sensor_type = itemView.findViewById<TextView>(R.id.sensor_type)
            val sensor_rssi = itemView.findViewById<TextView>(R.id.sensor_rssi)
            val sensor_sample_date = itemView.findViewById<TextView>(R.id.sensor_sample_date)
            var cap_rv = itemView.findViewById<RecyclerView>(R.id.capabilities_rv)
            var llm = LinearLayoutManager(itemView.context)
        }

        override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): SensorViewHolder {
            val v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sensor_card, viewGroup, false)
            return SensorViewHolder(v)
        }

        override fun getItemCount(): Int {
            return sensors.size
        }

        override fun onBindViewHolder(vh: SensorViewHolder, i: Int) {
            val sensor = sensors.values.elementAt(i)
            vh.mac_address.text = sensor.getString("mac_address")
            vh.sensor_type.text = sensor.getString("sensor_type")
            vh.sensor_rssi.text = sensor.getString("rssi")
            var timestamp = Date(sensor.getLong("timestamp"))
            vh.sensor_sample_date.text = formatter.format(timestamp)
            vh.cap_rv.layoutManager = vh.llm
            var cap_adapter = CapAdapter(sensor.getJSONArray("data"));
            vh.cap_rv.adapter = cap_adapter ;
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rv = findViewById<View>(R.id.rv) as RecyclerView
        var llm = LinearLayoutManager(this)
        rv.layoutManager = llm

//        addresses_filter.add("50:20:6B:03:07:08")
//        device_names_filter.add("XTEL-BEACON")

        adapter = SensorAdapter(sensors);
        rv.adapter = adapter;
        fab.setOnClickListener { view ->
            if (toggleBle()) {
                fab.setImageResource(android.R.drawable.ic_media_pause)
            } else {
                fab.setImageResource(android.R.drawable.ic_media_play)
            }
        }
    }

    override fun onSensorsCleared() {
        adapter.notifyDataSetChanged()
    }

    override fun onNewSensor(index: Int, sensor: JSONObject) {
        adapter.notifyItemInserted(index)
    }

    override fun onUpdatedSensor(index: Int, sensor: JSONObject) {
        adapter.notifyItemChanged(index)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }

}
